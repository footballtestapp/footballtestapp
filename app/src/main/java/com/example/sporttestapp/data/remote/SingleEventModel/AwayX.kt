package com.example.sporttestapp.data.remote.SingleEventModel

data class AwayX(
    val substitution: String? = null,
    val substitution_player_id: String? = null,
    val time: String? = null
)