package com.example.sporttestapp.data.remote.EventsModel

data class HomeX(
    val substitution: String,
    val substitution_player_id: String,
    val time: String
)