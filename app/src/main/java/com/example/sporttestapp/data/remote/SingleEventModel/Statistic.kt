package com.example.sporttestapp.data.remote.SingleEventModel

data class Statistic(
    val away: String? = null,
    val home: String? = null,
    val type: String? = null
)