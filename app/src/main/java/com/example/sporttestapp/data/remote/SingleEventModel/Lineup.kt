package com.example.sporttestapp.data.remote.SingleEventModel

data class Lineup(
    val away: Away? = Away(),
    val home: Home? = Home()
)