package com.example.sporttestapp.data.remote.SingleEventModel

data class Substitutions(
    val away: List<AwayX>? = listOf(),
    val home: List<HomeX>? = listOf()
)