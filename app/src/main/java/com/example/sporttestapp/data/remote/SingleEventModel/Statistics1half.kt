package com.example.sporttestapp.data.remote.SingleEventModel

data class Statistics1half(
    val away: String? = null,
    val home: String? = null,
    val type: String? = null
)