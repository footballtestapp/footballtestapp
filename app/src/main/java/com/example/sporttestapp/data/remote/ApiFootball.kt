package com.example.sporttestapp.data.remote


import com.example.sporttestapp.core.utils.UtilsObj
import com.example.sporttestapp.data.remote.EventsModel.Events
import com.example.sporttestapp.data.remote.SingleEventModel.SingleEvent
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiFootball {
    @GET("/?action=get_events&from=2022-04-05&to=2023-04-05&league_id=152&APIkey=${UtilsObj.API_KEY}")
    suspend fun getEvents(): Events

    @GET("/?action=get_events&from=2022-04-05&to=2023-04-05&APIkey=${UtilsObj.API_KEY}")
    suspend fun getEventById(
        @Query("match_id") eventId: String
    ): SingleEvent
}