package com.example.sporttestapp.data.remote.SingleEventModel

data class Card(
    val away_fault: String? = null,
    val away_player_id: String? = null,
    val card: String? = null,
    val home_fault: String? = null,
    val home_player_id: String? = null,
    val info: String? = null,
    val score_info_time: String? = null,
    val time: String? = null
)