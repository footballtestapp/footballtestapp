package com.example.sporttestapp.data.remote.EventsModel

data class Lineup(
    val away: Away,
    val home: Home
)