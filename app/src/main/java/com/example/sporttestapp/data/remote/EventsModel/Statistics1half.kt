package com.example.sporttestapp.data.remote.EventsModel

data class Statistics1half(
    val away: String,
    val home: String,
    val type: String
)