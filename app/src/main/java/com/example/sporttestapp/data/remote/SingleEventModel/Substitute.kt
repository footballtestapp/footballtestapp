package com.example.sporttestapp.data.remote.SingleEventModel

data class Substitute(
    val lineup_number: String? = null,
    val lineup_player: String? = null,
    val lineup_position: String? = null,
    val player_key: String? = null
)