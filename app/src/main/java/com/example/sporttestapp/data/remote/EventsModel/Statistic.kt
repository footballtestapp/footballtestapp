package com.example.sporttestapp.data.remote.EventsModel

data class Statistic(
    val away: String,
    val home: String,
    val type: String
)