package com.example.sporttestapp.data.remote.SingleEventModel

data class Home(
    val coach: List<Coach>? = listOf(),
    val missing_players: List<Any>? = listOf(),
    val starting_lineups: List<StartingLineup>? = listOf(),
    val substitutes: List<Substitute>? = listOf()
)