package com.example.sporttestapp.data.remote.SingleEventModel

data class HomeX(
    val substitution: String? = null,
    val substitution_player_id: String? = null,
    val time: String? = null
)