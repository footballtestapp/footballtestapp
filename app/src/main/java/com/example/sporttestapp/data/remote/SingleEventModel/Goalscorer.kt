package com.example.sporttestapp.data.remote.SingleEventModel

data class Goalscorer(
    val away_assist: String? = null,
    val away_assist_id: String? = null,
    val away_scorer: String? = null,
    val away_scorer_id: String? = null,
    val home_assist: String? = null,
    val home_assist_id: String? = null,
    val home_scorer: String? = null,
    val home_scorer_id: String? = null,
    val info: String? = null,
    val score: String? = null,
    val score_info_time: String? = null,
    val time: String? = null
)