package com.example.sporttestapp.data.remote.EventsModel

data class Substitutions(
    val away: List<AwayX>,
    val home: List<HomeX>
)