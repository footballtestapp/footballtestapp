package com.example.sporttestapp.data.remote.EventsModel

data class Home(
    val coach: List<Coach>,
    val missing_players: List<Any>,
    val starting_lineups: List<StartingLineup>,
    val substitutes: List<Substitute>
)