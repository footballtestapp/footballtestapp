package com.example.sporttestapp.data.remote.SingleEventModel

data class Away(
    val coach: List<Coach?>? = null,
    val missing_players: List<Any?>? = null,
    val starting_lineups: List<StartingLineup?>? = null,
    val substitutes: List<Substitute?>? = null
)