package com.example.sporttestapp.data.remote.EventsModel

data class AwayX(
    val substitution: String,
    val substitution_player_id: String,
    val time: String
)