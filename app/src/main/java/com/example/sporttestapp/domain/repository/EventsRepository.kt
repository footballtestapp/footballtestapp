package com.example.sporttestapp.domain.repository

import com.example.sporttestapp.data.remote.ApiFootball

class EventsRepository(private val apiFootball: ApiFootball) {
    suspend fun getEvents() = apiFootball.getEvents()

    suspend fun getEventById(eventId: String) = apiFootball.getEventById(eventId)
}