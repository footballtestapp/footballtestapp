package com.example.sporttestapp.core.di

import android.util.Log
import com.example.sporttestapp.data.remote.ApiFootball
import com.example.sporttestapp.domain.repository.EventsRepository
import com.example.sporttestapp.presentation.detailsscreen.DetailsViewModel
import com.example.sporttestapp.presentation.mainscreen.MainScreenViewModel
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val repositoryModule = module {
    factory { EventsRepository(get()) }
}

val viewModelModule = module {
    viewModel { MainScreenViewModel(get()) }
    viewModel { DetailsViewModel(get()) }
}

val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideApiFootball(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("https://apiv3.apifootball.com")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}

fun provideApiFootball(retrofit: Retrofit): ApiFootball = retrofit.create(ApiFootball::class.java)