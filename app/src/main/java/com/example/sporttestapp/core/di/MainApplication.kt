package com.example.sporttestapp.core.di

import android.app.Application
import com.example.sporttestapp.core.utils.UtilsObj.ONESIGNAL_APP_ID
import com.onesignal.OneSignal
import com.onesignal.debug.LogLevel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.scope.newScope
import org.koin.core.context.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()


        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(
                listOf(
                    repositoryModule,
                    viewModelModule,
                    networkModule
                )
            )
        }
    }
}