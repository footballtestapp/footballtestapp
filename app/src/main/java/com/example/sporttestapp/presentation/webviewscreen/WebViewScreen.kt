package com.example.sporttestapp.presentation.webviewscreen

import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import com.example.sporttestapp.core.utils.UtilsObj
import com.onesignal.OneSignal
import com.onesignal.debug.LogLevel

@Composable
fun WebViewScreen() {

    OneSignal.Debug.logLevel = LogLevel.VERBOSE

    OneSignal.initWithContext(LocalContext.current, UtilsObj.ONESIGNAL_APP_ID)

    OneSignal.User.pushSubscription.optIn()


    val mUrl = "https://www.google.com"

    AndroidView(factory = {
        WebView(it).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            webViewClient = WebViewClient()
            loadUrl(mUrl)
        }
    }, update = {
        it.loadUrl(mUrl)
    })
}
