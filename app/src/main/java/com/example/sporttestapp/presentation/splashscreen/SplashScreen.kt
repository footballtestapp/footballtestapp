package com.example.sporttestapp.presentation.splashscreen

import android.window.SplashScreen
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.sporttestapp.R
import com.example.sporttestapp.presentation.navigation.Screen

@Composable
fun SplashScreen(
    navController: NavController
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceAround,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
    ) {
        Image(
            painter = painterResource(id = R.drawable.baseline_sports_volleyball_24),
            contentDescription = "logo"
        )
        Text(
            text = "WELCOME",
            fontSize = 70.sp,
            fontWeight = FontWeight.Bold
        )
        Row() {
            Button(onClick = { navController.navigate(route = Screen.Main.route) }) {
                Text("START")
            }
            Spacer(modifier = Modifier.padding(10.dp))
            Button(onClick = { navController.navigate(route = Screen.WebView.route) }) {
                Text("WEB VIEW")
            }
        }

    }

}