package com.example.sporttestapp.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.sporttestapp.presentation.contentlistscreen.ContentListScreen
import com.example.sporttestapp.presentation.detailsscreen.DetailsScreen
import com.example.sporttestapp.presentation.mainscreen.EventsList
import com.example.sporttestapp.presentation.mainscreen.MainScreenViewModel
import com.example.sporttestapp.presentation.navigation.Screen
import com.example.sporttestapp.presentation.splashscreen.SplashScreen
import com.example.sporttestapp.presentation.theme.SportTestAppTheme
import com.example.sporttestapp.presentation.webviewscreen.WebViewScreen
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SportTestAppTheme {
                val navController = rememberNavController()
                val viewModel: MainScreenViewModel = koinViewModel()
                NavHost(navController = navController, startDestination = Screen.Splash.route) {
                    composable(
                        route = Screen.Splash.route
                    ) {
                        SplashScreen(navController)
                    }

                    composable(
                        route = Screen.Main.route
                    ) {
                        EventsList(viewModel = viewModel, navController = navController)
                    }
                    composable(
                        route = Screen.ContentList.route
                    ) {
                        ContentListScreen(navController = navController)
                    }
                    composable(
                        route = "${Screen.Details.route}/{eventId}"
                    ) {
                        val eventId = it.arguments?.getString("eventId")
                        eventId?.let {
                            DetailsScreen(eventId = eventId)
                        }
                    }
                    composable(
                        route = Screen.WebView.route
                    ) {
                        WebViewScreen()
                    }

                }
            }
        }
    }
}






