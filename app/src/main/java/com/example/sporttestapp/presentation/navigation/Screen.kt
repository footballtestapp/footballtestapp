package com.example.sporttestapp.presentation.navigation

sealed class Screen(val route: String){
    object Splash: Screen("splash_screen")
    object Main: Screen("main_screen")
    object WebView: Screen("webview_screen")
    object ContentList: Screen ("content_list_screen")
    object Details: Screen("details_screen")
}
