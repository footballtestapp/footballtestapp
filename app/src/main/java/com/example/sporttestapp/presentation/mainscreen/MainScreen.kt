package com.example.sporttestapp.presentation.mainscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.sporttestapp.data.remote.EventsModel.EventsItem
import com.example.sporttestapp.presentation.navigation.Screen
import org.koin.androidx.compose.koinViewModel

@Composable
fun EventsList(viewModel: MainScreenViewModel = koinViewModel(), navController: NavController) {


    val textState = viewModel.events.collectAsState().value
    when (textState) {
        is MainScreenState.Loading -> {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }
        }

        is MainScreenState.Success -> {
            Column {
                Text(
                    text = "Events",
                    fontSize = 40.sp,
                    fontWeight = FontWeight.Bold
                )

                LazyColumn() {

                    items(textState.events) {
                        Card(
                            elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                            modifier = Modifier
                                .padding(5.dp)
                                .clickable {
                                    navController.navigate(
                                        Screen.Details.route + "/{eventId}".replace(
                                            oldValue = "{eventId}",
                                            newValue = it.match_id
                                        )
                                    )
                                },
                            shape = MaterialTheme.shapes.medium,
                            colors = CardDefaults.cardColors(containerColor = Color.White)
                        ) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.SpaceAround
                            ) {
                                LeagueInfoRow(it)
                                MatchInfoRow(it)
                                Spacer(modifier = Modifier.padding(10.dp))
                            }


                        }

                    }
                }

            }
        }

        is MainScreenState.Error -> {

        }

    }


}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun LeagueInfoRow(eventsItem: EventsItem) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically

    ) {
        GlideImage(
            modifier = Modifier
                .padding(5.dp),
            model = eventsItem.league_logo,
            contentDescription = "",
            contentScale = ContentScale.Fit
        )
        Text(
            text = "${eventsItem.league_name}, ${eventsItem.country_name}",
            modifier = Modifier.fillMaxSize(),
            fontSize = 10.sp
        )

    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MatchInfoRow(eventsItem: EventsItem) {

    Column() {

        Text(
            text = eventsItem.match_date,
            fontSize = 8.sp,
            color = Color.Gray,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .align(Alignment.End)
                .padding(end = 8.dp)
        )
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically
        ) {
            TeamInfo(eventsItem.team_home_badge, eventsItem.match_hometeam_name)
            Text(
                text = "${eventsItem.match_hometeam_score}:${eventsItem.match_awayteam_score}",
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold
            )
            TeamInfo(eventsItem.team_away_badge, eventsItem.match_awayteam_name)

        }
    }

}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun TeamInfo(teamLogo: String, teamName: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        GlideImage(
            modifier = Modifier
                .padding(5.dp),
            model = teamLogo,
            contentDescription = "",
            contentScale = ContentScale.Fit
        )
        Text(
            text = teamName,
            textAlign = TextAlign.Center,
            fontSize = 8.sp
        )
    }

}