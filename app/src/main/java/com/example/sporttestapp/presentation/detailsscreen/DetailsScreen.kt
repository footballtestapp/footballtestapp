package com.example.sporttestapp.presentation.detailsscreen

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.sporttestapp.data.remote.SingleEventModel.Goalscorer
import com.example.sporttestapp.data.remote.SingleEventModel.SingleEventItem
import com.example.sporttestapp.presentation.mainscreen.TeamInfo
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun DetailsScreen(viewModel: DetailsViewModel = koinViewModel(), eventId: String) {
    val textState = viewModel.eventDetails.collectAsState()
    LaunchedEffect(Unit) {
        viewModel.getEventById(eventId)
    }
    Box(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        Column() {
            textState.value?.let {
                DetailsLeagueInfo(it)
                DetailsMatchInfo(it)
                if (!it.cards.isNullOrEmpty()) Cards(it)
                Substitutions(it)
                if (!it.goalscorer.isNullOrEmpty()) DetailsGoalScorer(it)

            }

        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun DetailsLeagueInfo(eventItem: SingleEventItem) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        GlideImage(
            model = eventItem?.league_logo,
            contentDescription = "",
            modifier = Modifier
                .width(50.dp)
                .height(50.dp)
        )
        Text(
            text = eventItem.league_name.toString(),
            fontSize = 40.sp,
            fontWeight = FontWeight.Bold
        )
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun DetailsMatchInfo(eventItem: SingleEventItem) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = eventItem.match_date.toString(),
            fontSize = 12.sp,
            fontWeight = FontWeight.Bold
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceAround,
            modifier = Modifier.fillMaxWidth()
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                GlideImage(
                    model = eventItem?.team_home_badge,
                    contentDescription = "",
                    modifier = Modifier
                        .width(70.dp)
                        .height(70.dp)
                )
                Text(
                    text = eventItem.match_hometeam_name.toString(),
                    fontSize = 12.sp
                )
            }

            Text(
                text = "${eventItem?.match_hometeam_score}:${eventItem?.match_awayteam_score}",
                fontSize = 80.sp,
                fontWeight = FontWeight.Bold
            )
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                GlideImage(
                    model = eventItem?.team_away_badge,
                    contentDescription = "",
                    modifier = Modifier
                        .width(70.dp)
                        .height(70.dp)
                )
                Text(
                    text = eventItem.match_awayteam_name.toString(),
                    fontSize = 12.sp
                )
            }

        }
    }

}

@Composable
fun Cards(eventItem: SingleEventItem) {
    Column() {
        Text(
            text = "Cards",
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold
        )
        Column() {
            eventItem.cards?.let { cards ->
                repeat(cards.toList().size) {
                    Card(
                        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                        modifier = Modifier
                            .padding(4.dp),
                        shape = MaterialTheme.shapes.small,
                        colors = CardDefaults.cardColors(containerColor = Color.White)
                    ) {
                        Column(
                            horizontalAlignment = Alignment.Start,
                            verticalArrangement = Arrangement.Center,
                            modifier = Modifier.padding(6.dp)
                        ) {

                            Row(
                                modifier = Modifier
                                    .padding(4.dp)
                                    .fillMaxSize()
                            ) {
                                when (cards[it].card) {
                                    "yellow card" -> {
                                        Canvas(modifier = Modifier.size(20.dp), onDraw = {
                                            drawRect(color = Color.Yellow)
                                        })
                                    }

                                    "red card" -> {
                                        Canvas(modifier = Modifier.size(20.dp), onDraw = {
                                            drawRect(color = Color.Red)
                                        })
                                    }
                                }

                                Text(
                                    text = "${cards[it].time} minute",
                                    modifier = Modifier.padding(start = 4.dp)
                                )

                            }
                            var homeFault: String = cards[it].home_fault.toString()
                            var awayFault: String = cards[it].away_fault.toString()
                            cards[it].home_fault?.ifEmpty {
                                homeFault = "no"
                            }
                            cards[it].away_fault?.ifEmpty {
                                homeFault = "no"
                            }
                            Text(
                                text = "Home Fault: $homeFault",
                                fontSize = 12.sp
                            )
                            Text(
                                text = "Away Fault: $awayFault",
                                fontSize = 12.sp
                            )
                        }
                    }
                }
            }
        }
    }


}

@Composable
fun Substitutions(eventItem: SingleEventItem) {
    Column() {
        Text(
            text = "Substitutions",
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold
        )
        Card(
            elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
            modifier = Modifier
                .padding(4.dp),
            shape = MaterialTheme.shapes.small,
            colors = CardDefaults.cardColors(containerColor = Color.White)
        ) {

            Row(
                modifier = Modifier
                    .height(100.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Column() {
                    Text(text = "Home:")
                    Column() {

                        eventItem.substitutions?.home.let { sub ->
                            repeat(sub!!.toList().size) {
                                Text(
                                    text = "${sub[it].time} ${sub[it].substitution}",
                                    fontSize = 8.sp
                                )
                            }
                        }
                    }

                }
                Divider(
                    color = Color.Black, modifier = Modifier
                        .fillMaxHeight()
                        .width(1.dp)
                )
                Column() {

                    Text(text = "Away:")
                    Column() {
                        eventItem.substitutions?.away.let { sub ->
                            repeat(sub!!.toList().size) {
                                Text(
                                    text = "${sub[it].time} ${sub[it].substitution}",
                                    fontSize = 8.sp
                                )
                            }
                        }
                    }
                }

            }
        }
    }
}

@Composable
fun DetailsGoalScorer(eventItem: SingleEventItem) {
    Column {
        Text(
            text = "Goalscorer",
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold
        )
        Column() {
            Card(
                elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxWidth(),
                shape = MaterialTheme.shapes.small,
                colors = CardDefaults.cardColors(containerColor = Color.White)
            ) {

                eventItem.goalscorer?.let { goalscorer ->
                    repeat(goalscorer.size) {
                        val homeScorer = goalscorer[it].home_scorer.toString()
                        val awayScorer = goalscorer[it].away_scorer.toString()
                        var scorer = homeScorer.ifEmpty { awayScorer }

                        Text(
                            text = "${goalscorer[it].time} minute, ${scorer}",
                            modifier = Modifier.padding(4.dp)
                        )

                    }
                }
            }

        }
    }
}