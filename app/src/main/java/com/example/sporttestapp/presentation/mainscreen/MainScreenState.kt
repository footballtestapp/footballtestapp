package com.example.sporttestapp.presentation.mainscreen

import com.example.sporttestapp.data.remote.EventsModel.EventsItem

sealed class MainScreenState {
    object Loading : MainScreenState()
    data class Success(val events: ArrayList<EventsItem>) : MainScreenState()
    object Error : MainScreenState()
}
