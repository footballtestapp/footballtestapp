package com.example.sporttestapp.presentation.detailsscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sporttestapp.data.remote.SingleEventModel.SingleEventItem
import com.example.sporttestapp.domain.repository.EventsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailsViewModel(
    private val eventsRepository: EventsRepository,

    ) : ViewModel() {
    val _eventDetails = MutableStateFlow<SingleEventItem?>(null)
    val eventDetails = _eventDetails.asStateFlow()

    suspend fun getEventById(eventId: String) = viewModelScope.launch {
        _eventDetails.value = eventsRepository.getEventById(eventId).first()
        //Log.d("log from viewmodel", eventDetails.value.toString())
    }
}