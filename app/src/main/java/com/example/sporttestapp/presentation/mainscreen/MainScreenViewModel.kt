package com.example.sporttestapp.presentation.mainscreen

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sporttestapp.data.remote.EventsModel.Events
import com.example.sporttestapp.domain.repository.EventsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainScreenViewModel(
    private val eventsRepository: EventsRepository
) : ViewModel() {
    private val _events = MutableStateFlow<MainScreenState>(MainScreenState.Loading)
    val events = _events.asStateFlow()

    init {
        getEvents()
    }

    fun getEvents() = viewModelScope.launch {
        //_events.value = eventsRepository.getEvents()
        _events.value = MainScreenState.Success(eventsRepository.getEvents())


    }
}
